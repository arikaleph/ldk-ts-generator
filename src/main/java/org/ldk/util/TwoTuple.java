package org.ldk.util;

public class TwoTuple<A, B> {
    public A a;
    public B b;
    public TwoTuple(A a, B b) {
        this.a = a;
        this.b = b;
    }
}
