package org.ldk.util;

public class ThreeTuple<A, B, C> {
    public A a;
    public B b;
    public C c;
    public ThreeTuple(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
